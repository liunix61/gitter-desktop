'use strict';

const log = require('loglevel');
const opn = require('opn');

const notifier = require('../notifier');

// Check for if the interval has passed every 10 minutes
const CHECK_TIME_POLLING_RATE = 10 * 60 * 1000;
// Only show a notification to update every 72 hours
const TIME_BETWEEN_NOTIFICATIONS = 72 * 60 * 60 * 1000;

function notifyAboutVersionAvailable(version) {
  log.info(`Showing notification for new version: ${version}`);
  notifier({
    title: `Gitter ${version} Available`,
    message: 'Head over to gitter.im/apps to update.',
    click() {
      log.info('Update notification clicked, opening gitter.im/apps');
      opn('https://gitter.im/apps');
    },
  });
}

let lastNotifyUpdateTime = -1;
function setupUpdateNotifications(version) {
  const tryNotification = () => {
    const timeSinceNotify = Date.now() - lastNotifyUpdateTime;

    if (timeSinceNotify >= TIME_BETWEEN_NOTIFICATIONS) {
      notifyAboutVersionAvailable(version);
      lastNotifyUpdateTime = Date.now();
    }
  };

  tryNotification();
  setInterval(tryNotification, CHECK_TIME_POLLING_RATE);
}

module.exports = setupUpdateNotifications;
